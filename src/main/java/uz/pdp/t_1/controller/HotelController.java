package uz.pdp.t_1.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import uz.pdp.t_1.entity.Hotel;
import uz.pdp.t_1.repository.HotelRepository;

import java.util.List;

@RestController
@RequestMapping("/hotel")
public class HotelController {
    @Autowired
    HotelRepository hotelRepository;

    @GetMapping
    public List<Hotel> getHotels(){
        return hotelRepository.findAll();
    }

    @GetMapping("/{id}")
    public Hotel getHotel(@PathVariable Integer id){
        return hotelRepository.findById(id).orElseThrow(() -> new IllegalStateException("Hotel not found"));
    }
    @DeleteMapping("/{id}")
    public String deleteHotel(@PathVariable Integer id){
        hotelRepository.deleteById(id);
        return "Hotel deleted";
    }

    @PostMapping
    public String addHotel(@RequestBody Hotel hotel){
        Hotel hotel1=new Hotel();
        hotel1.setName(hotel.getName());
        hotelRepository.save(hotel1);
        return "Hotel added";
    }
    @PutMapping("/{id}")
    public String editHotel(@PathVariable Integer id,@RequestBody Hotel newhotel){
        Hotel hotel = hotelRepository.getById(id);
        hotel.setName(newhotel.getName());
        hotelRepository.save(hotel);
        return "Hotel edited";
    }
}
