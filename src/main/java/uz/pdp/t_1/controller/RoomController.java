package uz.pdp.t_1.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import uz.pdp.t_1.entity.Hotel;
import uz.pdp.t_1.entity.Room;
import uz.pdp.t_1.payload.RoomDto;
import uz.pdp.t_1.repository.HotelRepository;
import uz.pdp.t_1.repository.RoomRepository;

import java.util.List;

@RestController
@RequestMapping("/room")
public class RoomController {
    @Autowired
    RoomRepository roomRepository;

    @Autowired
    HotelRepository hotelRepository;

    @GetMapping
    public List<Room> getRooms(){
        return roomRepository.findAll();
    }

    @GetMapping("/{id}")
    public Room getRoom(@PathVariable Integer id){
        return roomRepository.findById(id).orElseThrow(() -> new IllegalStateException("Room not found"));
    }
    @DeleteMapping("/{id}")
    public String deleteRoom(@PathVariable Integer id){
        roomRepository.deleteById(id);
        return "Room deleted";
    }
    @PostMapping
    public String addRoom(@RequestBody RoomDto roomDto){
        Hotel hotel = hotelRepository.getById(roomDto.getHotelId());
        Room room=new Room(roomDto.getNumber(),roomDto.getFloor(),roomDto.getSize(),hotel);
        roomRepository.save(room);
        return "Room added";
    }
    @PutMapping("/{id}")
    public String editRoom(@PathVariable Integer id,@RequestBody RoomDto roomDto){
        Room room = roomRepository.getById(id);
        room.setNumber(roomDto.getNumber());
        room.setFloor(roomDto.getFloor());
        room.setSize(roomDto.getSize());
        room.setHotel(hotelRepository.getById(roomDto.getHotelId()));
        roomRepository.save(room);
        return "Room edited";
    }

    @GetMapping("/getRoomsByHotelId/{hotelId}")
    public Page<Room> getRoomsByHotelId(@PathVariable Integer hotelId,@RequestParam int page){
        Pageable pageable= PageRequest.of(page,3);
        return roomRepository.findAllByHotelId(hotelId,pageable);
    }
}
