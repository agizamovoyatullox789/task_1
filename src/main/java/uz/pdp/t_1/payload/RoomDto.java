package uz.pdp.t_1.payload;

import lombok.Data;

@Data
public class RoomDto {
    private String number;
    private int floor;
    private int size;
    private int hotelId;
}
